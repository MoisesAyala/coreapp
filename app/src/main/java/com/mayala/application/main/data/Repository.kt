package com.mayala.application.main.data

import com.mayala.application.main.data.model.DataResponse
import com.mayala.application.main.data.network.DataService

class Repository {
    private val service = DataService()

    suspend fun getData(): DataResponse {
        return service.getAllData()
    }
}