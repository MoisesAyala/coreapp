package com.mayala.application.main.data.network

import com.mayala.application.BuildConfig
import com.mayala.application.main.core.RetrofitUtils
import com.mayala.application.main.data.model.DataResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DataService {

    private val retrofit = RetrofitUtils.retrofit(BuildConfig.APIURL)

    suspend fun getAllData(): DataResponse {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(ApiService::class.java).getData()
            return response.errorBody() ?: DataResponse(statusCode = 0, statusMessage = "err", false)
        }
    }
}