package com.mayala.application.main.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mayala.application.main.data.Repository
import com.mayala.application.main.data.model.DataResponse
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val repository = Repository()
    var data = MutableLiveData<DataResponse>()

    suspend fun getAllData() {
        viewModelScope.launch {
            data.postValue(repository.getData())
        }
    }
}