package com.mayala.application.main.data.model

import com.google.gson.annotations.SerializedName

data class DataResponse(
    @SerializedName("status_code")
    var statusCode: Int,
    @SerializedName("status_message")
    var statusMessage: String,
    @SerializedName("success")
    var success: Boolean
)