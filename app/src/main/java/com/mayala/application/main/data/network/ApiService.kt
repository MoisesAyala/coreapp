package com.mayala.application.main.data.network

import com.mayala.application.main.data.model.DataResponse
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("url/example")
    suspend fun getData(): Response<DataResponse>
}